import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:universities_app/features/universities/presentation/cubit/universities_cubit.dart';
import 'package:universities_app/routes/routes.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UniversitiesCubit>(
      create: (context) => UniversitiesCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: const SafeArea(
          child: Scaffold(
            body: Center(
              child: Text('Hello World!'),
            ),
          ),
        ),
        routes: getApplicationRoutes(),
        initialRoute: 'universities',
      ),
    );
  }
}
