import 'package:flutter/material.dart';
import 'package:universities_app/features/universities/presentation/pages/universities_detail.dart';
import 'package:universities_app/features/universities/presentation/pages/universities_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'universities': (BuildContext context) => const UniversitiesPage(),
    'detail': (BuildContext context) => const UniversitiesDetailPage(),
  };
}
