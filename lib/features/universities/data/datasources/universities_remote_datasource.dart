import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:universities_app/features/universities/data/models/universities_model.dart';

class UniversitiesRemoteDatasources {
  Future getUniversitiesFromApi() async {
    try {
      List<UniversitiesModel> universities = [];
      final url = Uri.https(
        'tyba-assets.s3.amazonaws.com',
        'FE-Engineer-test/universities.json',
      );
      final response = await http.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      );

      if (response.statusCode != 200) {
        throw const SocketException('Error en el servidor');
      }
      var data = jsonDecode(response.body);
      for (var item in data) {
        universities.add(UniversitiesModel.fromMap(item));
      }
      // UniversitiesModel universities =
      //     UniversitiesModel.fromJson(response.body);
      return universities;
    } on SocketException catch (e) {
      throw Failure(e.message);
    }
  }
}

class Failure {
  final String message;

  Failure(this.message);

  @override
  String toString() => message;
}
