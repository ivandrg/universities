// To parse this JSON data, do
//
//     final universitiesModel = universitiesModelFromMap(jsonString);

import 'dart:convert';

class UniversitiesModel {
  UniversitiesModel({
    required this.alphaTwoCode,
    required this.domains,
    required this.country,
    required this.stateProvince,
    required this.webPages,
    required this.name,
  });

  final AlphaTwoCode alphaTwoCode;
  final List<String> domains;
  final Country country;
  final String stateProvince;
  final List<String> webPages;
  final String name;

  factory UniversitiesModel.fromJson(String str) =>
      UniversitiesModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UniversitiesModel.fromMap(Map<String, dynamic> json) =>
      UniversitiesModel(
        alphaTwoCode: alphaTwoCodeValues.map[json["alpha_two_code"]]!,
        domains: List<String>.from(json["domains"].map((x) => x)),
        country: countryValues.map[json["country"]]!,
        stateProvince: json["state-province"] ?? '',
        webPages: List<String>.from(json["web_pages"].map((x) => x)),
        name: json["name"],
      );

  Map<String, dynamic> toMap() => {
        "alpha_two_code": alphaTwoCodeValues.reverse[alphaTwoCode],
        "domains": List<dynamic>.from(domains.map((x) => x)),
        "country": countryValues.reverse[country],
        "state-province": stateProvince,
        "web_pages": List<dynamic>.from(webPages.map((x) => x)),
        "name": name,
      };
}

enum AlphaTwoCode { us }

final alphaTwoCodeValues = EnumValues({"US": AlphaTwoCode.us});

enum Country { unitedStates }

final countryValues = EnumValues({"United States": Country.unitedStates});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
