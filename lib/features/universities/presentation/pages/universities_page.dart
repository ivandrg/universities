import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:universities_app/features/universities/presentation/cubit/universities_cubit.dart';
import 'package:universities_app/features/universities/presentation/cubit/universities_state.dart';

class UniversitiesPage extends StatefulWidget {
  const UniversitiesPage({Key? key}) : super(key: key);

  @override
  State<UniversitiesPage> createState() => _UniversitiesPageState();
}

class _UniversitiesPageState extends State<UniversitiesPage> {
  @override
  void initState() {
    super.initState();
    context.read<UniversitiesCubit>().getUniversities();
  }

  bool mode = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Universidades'),
        actions: [
          SizedBox(
            width: 150,
            child: ListTile(
              leading: const Icon(Icons.list),
              title: Switch(
                value: mode,
                activeColor: Colors.red,
                onChanged: (bool value) {
                  setState(() {
                    mode = value;
                  });
                },
              ),
              trailing: const Icon(Icons.grid_on),
            ),
          ),
        ],
      ),
      body: BlocBuilder<UniversitiesCubit, UniversitiesState>(
        builder: (context, state) {
          if (state is GetUniversitiesLoaded) {
            if (mode) {
              return GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 3 / 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                ),
                itemCount: state.universities.length,
                itemBuilder: (BuildContext ctx, index) {
                  return Card(
                    child: Center(
                      child: ListTile(
                        onTap: () => Navigator.pushNamed(
                          context,
                          'detail',
                          arguments: state.universities[index],
                        ),
                        title: Text(
                          state.universities[index].name,
                        ),
                        subtitle:
                            Text(state.universities[index].alphaTwoCode.name),
                      ),
                    ),
                  );
                },
              );
            } else {
              return ListView.builder(
                itemCount: state.universities.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () => Navigator.pushNamed(
                      context,
                      'detail',
                      arguments: state.universities[index],
                    ),
                    title: Text(state.universities[index].name),
                    trailing: Chip(
                      label: Text(state.universities[index].alphaTwoCode.name),
                    ),
                  );
                },
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
