import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../data/models/universities_model.dart';

class UniversitiesDetailPage extends StatefulWidget {
  const UniversitiesDetailPage({Key? key}) : super(key: key);

  @override
  State<UniversitiesDetailPage> createState() => _UniversitiesDetailPageState();
}

class _UniversitiesDetailPageState extends State<UniversitiesDetailPage> {
  XFile? image;
  final ImagePicker _picker = ImagePicker();
  String students = '';

  @override
  Widget build(BuildContext context) {
    final data =
        ModalRoute.of(context)!.settings.arguments as UniversitiesModel;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Informacion de la Universidad'),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 50,
          ),
          if (image != null)
            Image.file(
              File(image!.path),
              height: 250,
            )
          else
            const Icon(
              Icons.image,
              size: 200,
              color: Colors.black12,
            ),
          const SizedBox(
            height: 20,
          ),
          ListTile(
            onTap: () async {
              image = await _picker.pickImage(source: ImageSource.camera);
              setState(() {});
            },
            leading: const Icon(
              Icons.camera_alt_outlined,
              size: 45,
            ),
            title: const Text('Tomar Foto'),
          ),
          ListTile(
            onTap: () async {
              image = await _picker.pickImage(source: ImageSource.gallery);
              setState(() {});
            },
            leading: const Icon(
              Icons.image,
              size: 45,
            ),
            title: const Text('Adjuntar Imagen'),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: TextFormField(
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                hintText: 'Numero de estudiantes',
              ),
              onChanged: (value) {
                students = value;
              },
              validator: (String? value) {
                return (value != null && value.contains('@'))
                    ? 'Do not use the @ char.'
                    : null;
              },
            ),
          ),
          ListTile(
            leading: const Icon(
              Icons.person,
              size: 45,
            ),
            title: const Text('Nombre'),
            subtitle: Text(data.name),
          ),
          ListTile(
            leading: const Icon(
              Icons.map,
              size: 45,
            ),
            title: const Text('Pais'),
            subtitle: Text(data.country.name),
          ),
          ListTile(
            leading: const Icon(
              Icons.code,
              size: 45,
            ),
            title: const Text('Codigo'),
            subtitle: Text(data.alphaTwoCode.name),
          ),
          ListTile(
            leading: const Icon(
              Icons.domain,
              size: 45,
            ),
            title: const Text('Dominio'),
            subtitle: Text(data.domains[0]),
          ),
          ListTile(
            leading: const Icon(
              Icons.web,
              size: 45,
            ),
            title: const Text('Webpage'),
            subtitle: Text(data.webPages[0]),
          ),
        ],
      ),
    );
  }
}
