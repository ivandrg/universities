import 'package:bloc/bloc.dart';
import 'package:universities_app/features/universities/presentation/cubit/universities_state.dart';

import '../../data/datasources/universities_remote_datasource.dart';

class UniversitiesCubit extends Cubit<UniversitiesState> {
  UniversitiesCubit() : super(UniversitiesInitial());
  final universitiesDatasource = UniversitiesRemoteDatasources();

  Future<void> getUniversities() async {
    try {
      emit(GetUniversitiesLoading());
      final universities =
          await universitiesDatasource.getUniversitiesFromApi();
      emit(GetUniversitiesLoaded(universities));
    } on Failure catch (e) {
      emit(GetUniversitiesError(e.message));
    }
  }
}
