import 'package:flutter/foundation.dart';

import '../../data/models/universities_model.dart';

abstract class UniversitiesState {}

class UniversitiesInitial extends UniversitiesState {}

class GetUniversitiesError extends UniversitiesState {
  final String message;

  GetUniversitiesError(this.message);
}

class GetUniversitiesLoading extends UniversitiesState {}

class GetUniversitiesLoaded extends UniversitiesState {
  final List<UniversitiesModel> universities;

  GetUniversitiesLoaded(this.universities);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GetUniversitiesLoaded &&
        listEquals(other.universities, universities);
  }

  @override
  int get hashCode => universities.hashCode;
}
